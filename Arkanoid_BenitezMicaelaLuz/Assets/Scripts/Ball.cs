﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameManagerScript;
using PlayerScript;

// Movimiento y colisiones de la pelota

namespace BallScript
{
    public class Ball : MonoBehaviour
    {
        public float speedBall = 2.5f;
        private Vector3 direction;

        private float initialPosY;
        private float initialPosZ;
        private float offSet = 0.15f;

        private float rayDistance = 0.5f;

        private Player player;

        private void Awake()
        {
            player = FindObjectOfType<Player>();

            initialPosY = 0.25f;
            initialPosZ = player.transform.position.z - player.transform.lossyScale.z / 2 - transform.lossyScale.z / 2 - offSet;
        }

        private void Update()
        {
            DrawRaycast(transform.forward);
            DrawRaycast(-transform.forward);
            DrawRaycast(transform.right);
            DrawRaycast(-transform.right);

            if (Input.GetKeyDown(KeyCode.Space))
                GameManager.Get().ballMoving = true;

            if (GameManager.Get().ballMoving)
            {
                transform.position += direction * speedBall * Time.deltaTime;
            }
            else
            {
                transform.position = new Vector3(player.transform.position.x, initialPosY, initialPosZ);
                direction = new Vector3(0, 0, -1.0f);
            }
        }

        private void OnCollisionEnter(Collision collision)                                                                                                                                                                         
        {
            if (collision.transform.gameObject.tag == "VerticalWall")
                direction.x *= -1.0f;
            
            if (collision.transform.gameObject.tag == "HorizontalWall")
                direction.z *= -1.0f;
            
            if (collision.transform.gameObject.tag == "Player")
            {
                if (transform.position.z < player.transform.position.z - player.transform.lossyScale.z / 2)
                {
                    direction.z *= -1.0f;
                    direction.x = (transform.position.x - player.transform.position.x);
                    direction = Vector3.Normalize(direction);
            
                    speedBall += 0.5f;
                    if (speedBall >= 5.0f)
                        speedBall = 5.0f;
                }
            }
            
            if (collision.transform.gameObject.tag == "Brick")
            {
                CheckRaycastHit();

                Destroy(collision.gameObject);
                GameManager.Get().AddScore();
                GameManager.Get().CheckWinnerCondition();
            }
        }

        private void CheckRaycastHit()   //   Chequeo con que lado del bloque choca la pelota para saber que direccion invertir
        {
            RaycastHit hit;

            if (transform.position.z < player.transform.position.z - player.transform.lossyScale.z / 2)   // Si la pelota sigue viva
            {
                if (Physics.Raycast(transform.position, transform.forward, out hit, rayDistance))
                {
                    if (direction.z > 0)
                        direction.z *= -1.0f;
                }
                else if (Physics.Raycast(transform.position, -transform.forward, out hit, rayDistance))
                {
                    if (direction.z < 0)
                        direction.z *= -1.0f;
                }
                else if (Physics.Raycast(transform.position, transform.right, out hit, rayDistance))
                {
                    if (direction.x > 0)
                        direction.x *= -1.0f;
                }
                else if (Physics.Raycast(transform.position, -transform.right, out hit, rayDistance))
                {
                    if (direction.x < 0)
                        direction.x *= -1.0f;
                }
                else
                {
                    direction.x *= -1.0f;
                    direction.z *= -1.0f;
                }
            }
        }

        private void DrawRaycast(Vector3 direction)
        {
            Debug.DrawRay(transform.position, direction * rayDistance, Color.black);
        }
    }
}