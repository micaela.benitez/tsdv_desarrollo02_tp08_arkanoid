﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameManagerScript;

// Le seteo el tamaño y color a cada bloque

namespace BrickScript
{
    public class Brick : MonoBehaviour
    {
        public Material[] materials;

        private Renderer render;

        private void Awake()
        {
            render = GetComponent<Renderer>();
        }

        private void Start()
        {
            transform.localScale = new Vector3(GameManager.Get().bricksWidth, transform.localScale.y, GameManager.Get().bricksHeight);
            render.material = materials[Random.Range(0, materials.Length)];
        }
    }
}