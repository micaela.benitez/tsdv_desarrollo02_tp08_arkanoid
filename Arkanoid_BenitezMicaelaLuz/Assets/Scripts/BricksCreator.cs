﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameManagerScript;

// Instancio los bloques en sus respectivas posiciones

namespace BricksCreatorScript
{
    public class BricksCreator : MonoBehaviour
    {
        public GameObject brickPrefab;

        private float brickPosY = 0.15f;

        private void Start()
        {
            for (int i = 0; i < GameManager.Get().bricksPerLine; i++)
            {
                for (int j = 0; j < GameManager.Get().bricksPerColumn; j++)
                {
                    float x = j * GameManager.Get().bricksWidth - (GameManager.Get().screenWidth / 2) + (GameManager.Get().bricksWidth / 2);
                    float z = i * GameManager.Get().bricksHeight - (GameManager.Get().screenHeight / 2) + (GameManager.Get().bricksHeight / 2);

                    GameObject go = Instantiate(brickPrefab, new Vector3(x, brickPosY, z), Quaternion.identity).gameObject;
                    go.transform.parent = gameObject.transform;
                }
            }
        }
    }
}