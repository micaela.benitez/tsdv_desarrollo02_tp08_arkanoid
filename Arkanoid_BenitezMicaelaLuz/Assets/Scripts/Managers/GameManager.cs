﻿using System;
using UnityEngine;
using LoaderManagerScript;
using MonoBehaviourSingletonScript;

namespace GameManagerScript
{
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        [Header("Player data")]
        public int totalLives = 3;
        [NonSerialized] public int lives;

        [NonSerialized] public int score;
        [NonSerialized] public int highscore;
        [NonSerialized] public bool winner;

        [NonSerialized] public bool ballMoving;

        [Header("Bricks data")]
        public int bricksPoints = 10;
        [Range(0, 5)] public int bricksPerLine = 5;
        [Range(0, 15)] public int bricksPerColumn = 10;
        [NonSerialized] public float bricksWidth;
        public float bricksHeight = 1;
        private int totalBricks;
        private int totalBricksAlive;

        private GameObject background;
        [NonSerialized] public float screenWidth;
        [NonSerialized] public float screenHeight;

        public void InitData()
        {
            lives = totalLives;
            ballMoving = false;
            score = 0;
            winner = true;

            background = GameObject.FindGameObjectWithTag("Background");
            screenWidth = background.transform.lossyScale.x;
            screenHeight = background.transform.lossyScale.z;

            bricksWidth = screenWidth / bricksPerColumn;

            totalBricks = bricksPerLine * bricksPerColumn;
            totalBricksAlive = totalBricks;

            highscore = PlayerPrefs.GetInt("Highscore");
        }

        public void Die()   // Si la pelota se pierde
        {
            ballMoving = false;
            lives--;
        }

        public void AddScore()   // Cada vez que la pelota choca con un bloque
        {
            score += bricksPoints;
            totalBricksAlive--;

            if (score > highscore)
            {
                highscore = score;
                PlayerPrefs.SetInt("Highscore", highscore);
                PlayerPrefs.Save();
            }
        }

        public void CheckWinnerCondition()
        {
            if (totalBricksAlive == 0 || lives == 0)
            {
                if (lives == 0)
                    winner = false;

                LoaderManager.Get().LoadScene("Result");
            }
        }
    }
}