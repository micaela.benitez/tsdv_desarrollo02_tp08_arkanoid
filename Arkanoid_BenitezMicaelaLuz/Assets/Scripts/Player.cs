﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameManagerScript;

// Movimiento del padle y chequeo si la pelota se perdio

namespace PlayerScript
{
    public class Player : MonoBehaviour
    {
        [Header("Player data")]
        public float speed = 5;
        private float maxPosX;

        [Header("Ball data")]
        public GameObject ball;

        private void Awake()
        {
            GameManager.Get().InitData();
        }

        private void Start()
        {
            maxPosX = (GameManager.Get().screenWidth / 2) - (transform.lossyScale.x / 2);
        }

        private void Update()
        {
            float hor = Input.GetAxis("Horizontal");
            
            Vector3 direction = new Vector3(hor, 0, 0);
            transform.position -= direction * speed * Time.deltaTime;

            if (transform.position.x > maxPosX)
                transform.position = new Vector3(maxPosX, transform.position.y, transform.position.z);
            else if (transform.position.x < -maxPosX)
                transform.position = new Vector3(-maxPosX, transform.position.y, transform.position.z);

            if (ball.transform.position.z > 5)
            {
                GameManager.Get().Die();
                GameManager.Get().CheckWinnerCondition();
            }
        }
    }
}