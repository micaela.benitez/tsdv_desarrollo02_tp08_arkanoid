﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using LoaderManagerScript;
using GameManagerScript;

namespace UIGameScript
{
    public class UIGame : MonoBehaviour
    {
        public TMP_Text score;
        public TMP_Text highscore;

        private float minLive = 0;
        private float maxLive = 0;
        private float totalLive = 0;
        public Image live;

        private void Start()
        {
            maxLive = GameManager.Get().totalLives;
        }

        private void Update()
        {
            score.text = "" + GameManager.Get().score;
            highscore.text = "" + GameManager.Get().highscore;

            totalLive = Mathf.Clamp(GameManager.Get().lives, minLive, maxLive);
            live.fillAmount = totalLive / maxLive;
        }

        public void LoadMenuScene()
        {
            LoaderManager.Get().LoadScene("MainMenu");
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}