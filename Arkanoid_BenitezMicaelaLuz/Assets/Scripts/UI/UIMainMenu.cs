﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LoaderManagerScript;

namespace UIMainMenuScript
{
    public class UIMainMenu : MonoBehaviour
    {
        public void LoadGameScene()
        {
            LoaderManager.Get().LoadScene("Game");
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}