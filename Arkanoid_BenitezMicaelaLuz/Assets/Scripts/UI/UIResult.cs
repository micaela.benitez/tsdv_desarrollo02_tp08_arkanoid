﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using LoaderManagerScript;
using GameManagerScript;

namespace UIResultScript
{
    public class UIResult : MonoBehaviour
    {
        public TMP_Text result;
        public TMP_Text score;
        public TMP_Text highscore;

        private void Start()
        {
            if (GameManager.Get().winner)
                result.text = "WINNER";
            else
                result.text = "LOSER";

            score.text = "Score: " + GameManager.Get().score;
            highscore.text = "Highscore: " + GameManager.Get().highscore;
        }

        public void LoadMenuScene()
        {
            LoaderManager.Get().LoadScene("MainMenu");
        }

        public void LoadGameScene()
        {
            LoaderManager.Get().LoadScene("Game");
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}